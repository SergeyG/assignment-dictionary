global find_word 
extern string_equals
 
section .rodata
error: db "No such word in dictionary", 0
 
section .text
 
find_word:
    xor rax, rax 
.loop:
    test rsi, rsi 
    jz .end 
    push rdi 
    push rsi 
    add rsi, 8
    call string_equals 
    pop rsi
    pop rdi
    test rax, rax 
    jne .found 
    mov rsi, [rsi] 
    jmp .loop 
.found:
    mov rax, rsi
.end:
    ret