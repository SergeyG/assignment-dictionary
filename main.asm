section .text

extern read_word
extern find_word
extern string_length
extern print_string
extern exit
 
 %define buffer_size 256
 
global _start
section .rodata

error: db "No such word in dictionary", 0
%include "words.inc"
 
section .text
_start:
    push rbp 
    mov rbp, rsp 
    sub rsp, buffer_size
    mov rdi, rsp 
    push rsi 
    mov rsi, buffer_size
    call read_word 
    mov rdi, rax 
    mov rsi, last_word 
    call find_word 
    test rax, rax 
    jz .end 
    add rax, 8 
    push rax 
    mov rax, [rsp] 
    mov rdi, rax 
    call string_length 
    pop rdi 
    add rdi, rax 
    inc rdi 
    call print_string 
    mov rsp, rbp 
    pop rsi
    pop rbp 
    call exit 
.end:
    mov rdi, error 
    call print_string
    mov rsp, rbp 
    pop rsi
    pop rbp 
    call exit 
