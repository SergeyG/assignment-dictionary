ELF = -f elf64
NASM = nasm
LD = ld


lib.o: lib.asm 
	$(NASM) $(ELF) -o lib.o lib.asm
    
dict.o: dict.asm 
	$(NASM) $(ELF) -o dict.o dict.asm

main.o: main.asm colon.inc words.inc
	$(NASM) $(ELF) -o main.o main.asm
 
main: main.o dict.o lib.o
	$(LD) -o main main.o dict.o lib.o

mapping.o: mapping.asm
	$(NASM) $(ELF) -o mapping.o mapping.asm

mapping: mapping.o lib.o
	$(LD) -o mapping mapping.o lib.o

clean: 
	rm -f *.o 
 
all: main
