section .text

global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parce_uint
global parse_int
global string_equals
global string_copy

global exit
 
%define stdout 1
%define stderr 2

exit:
  mov rax, 60
  syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .loop:
    cmp byte[rdi+rax], 0x0
    je .break
    inc rax
    jmp .loop
  .break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
  .print:
    mov rsi, rdi
    mov rdi, stdout
    mov rdx, rax
    mov rax, 0x1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 0x1
    mov rdi, stdout
    mov rsi, rsp
    mov rdx, 0x1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xa

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, rsp
    sub rsp, 0x16
    dec rcx
    mov [rcx], byte 0x0
    mov r10, 0xa
  .loop:
    xor rdx, rdx
    div r10
    add rdx, 0x30
    dec rcx
    mov [rcx], dl
    test rax, rax
    jne .loop
    mov rdi, rcx
    call print_string
    add rsp, 0x16
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
      test rdi, rdi 
      jns print_uint 
      push rdi 
      mov rdi, '-'
      call print_char 
      pop rdi 
      neg rdi 
      jmp print_uint 
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8
    mov rax, 0x1
  .loop:
    mov r8b, [rdi+rcx]
    cmp r8b, [rsi+rcx]
    jne .err
    cmp r8b, 0x0
    je .end
    inc rcx
    jmp .loop
  .err:
    xor rax, rax
  .end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0x0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 0x1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rsi-0x1          
    push rdi                
    xor r8, r8
    xor r9, r9
  .loop:
    call read_char          
    mov rdi, [rsp]
    mov rsi, [rsp+0x1]      
    cmp rax, 0x9
    je .check
    cmp rax, 0xa
    je .check
    cmp rax, 0x20
    je .check
    mov r9, 0x1            
    cmp rax, 0x0            
    je .check                
    cmp r8, rsi              
    je .err                 
                             
    mov [rdi+r8], al        
    inc r8                   
                            
    jmp .loop
  .check:
    test r9, r9             
    je .loop                
    mov [rdi+r8], byte 0x0  
    mov rax, rdi            
    mov rdx, r8             
    jmp .end
  .err:
    xor rax, rax
  .end:
    pop rdi                 
    pop rsi                 
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 0xa
  .loop:
    mov r9b, [rdi+r8]
    cmp r9b, 0x30
    jb .break
    cmp r9b, 0x39
    ja .break
    sub r9b, 0x30
    mul r10
    add rax, r9
    inc r8
    jmp .loop
  .break:
    mov rdx, r8
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 0x2d
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
  .loop:
    cmp rax, rdx
    je .err
    mov r8b, [rdi+rax]
    mov [rsi+rax], r8b
    test r8b, r8b
    je .break
    inc rax
    jmp .loop
  .err:
    xor rax, rax
  .break:
    ret

